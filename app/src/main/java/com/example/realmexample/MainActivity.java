package com.example.realmexample;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.io.File;
import java.util.Objects;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;
import io.realm.internal.IOException;

import static android.content.ContentValues.TAG;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnAdd, btnRead, btnUpdate, btnDelete, btnFilterByAge, btnBackUp;
    EditText inName, inAge, inSkill;
    TextView mTextView, txtFilterByAge;

    Realm mRealm;

    RealmResults<Employee> results;

    public static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE

    };

    private File EXPORT_REALM_PATH = Environment.
            getExternalStoragePublicDirectory
                    (Environment.DIRECTORY_DOWNLOADS);
    ;


    private String EXPORT_REALM_FILE_NAME = "backUpData.realm";
    private String IMPORT_REALM_FILE_NAME = "default.realm";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        Realm.init(getApplicationContext());

        RealmConfiguration configuration = new RealmConfiguration.Builder().
                deleteRealmIfMigrationNeeded().allowWritesOnUiThread(true).build();

        Realm.setDefaultConfiguration(configuration);


    }

    private void backUp(Realm realm) {

        realm = Realm.getDefaultInstance();


        checkStoragePermissions(this);
        // create a backup file
        File exportRealmFile = null;
        try {
            EXPORT_REALM_PATH.mkdirs();

           //exportRealmFile = new File(EXPORT_REALM_PATH, EXPORT_REALM_FILE_NAME);
            exportRealmFile = createImageFile();

            // if backup file already exists, delete it
            exportRealmFile.delete();

            // copy current realm to backup file
            realm.writeCopyTo(exportRealmFile);


        } catch (IOException e) {
            e.printStackTrace();
        }


        String msg = "File exported to Path: " + EXPORT_REALM_PATH + "/" + EXPORT_REALM_FILE_NAME;
        Toast.makeText(this.getApplicationContext(), msg, Toast.LENGTH_LONG).show();
        Log.d(TAG, msg);

        realm.close();
    }


    private File createImageFile() {

        return new File(Objects.requireNonNull(createFolder()).getPath(), "backUp.realm");
    }

    public File createFolder() {
        //File myFolder = new File(Environment.getExternalStorageDirectory(), "faraKhalili");
        File myFolder = new File(getExternalFilesDir(null), "FaraSample");
        if (!myFolder.exists()) {
            if (myFolder.mkdirs()) {
                return myFolder;
            } else {
                return null;
            }
        }
        return myFolder;
    }


    private void initViews() {
        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);
        btnRead = findViewById(R.id.btnRead);
        btnRead.setOnClickListener(this);
        btnUpdate = findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(this);
        btnDelete = findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(this);
        btnFilterByAge = findViewById(R.id.btnFilterByAge);
        btnFilterByAge.setOnClickListener(this);
        btnBackUp = findViewById(R.id.btn_backUp);
        btnBackUp.setOnClickListener(this);

        mTextView = findViewById(R.id.textViewEmployee);
        txtFilterByAge = findViewById(R.id.txtFilterByAge);

        inName = findViewById(R.id.inName);
        inAge = findViewById(R.id.inAge);
        inSkill = findViewById(R.id.inSkill);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnAdd:
                addEmployee();
                break;
            case R.id.btn_backUp:
                backUp(mRealm);
                break;
            case R.id.btnRead:
                readEmployeeRecords();

                break;
            case R.id.btnUpdate:
                updateEmployeeRecords();

                break;
            case R.id.btnDelete:
                deleteEmployeeRecord();

                break;
            case R.id.btnFilterByAge:
                filterByAge();

                break;
        }

    }


    private void addEmployee() {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    try {
                        if (!inName.getText().toString().trim().isEmpty()) {
                            Employee employee = new Employee();
                            employee.name = inName.getText().toString().trim();
                            if (!inAge.getText().toString().trim().isEmpty())
                                employee.age = Integer.parseInt(inAge.getText().toString().trim());
                            String languageKnown = inSkill.getText().toString().trim();

                            if (!languageKnown.isEmpty()) {
                                skill Skill = realm.where(skill.class).equalTo(com.example.
                                        realmexample.skill.PROPERTY_SKILL, languageKnown).findFirst();
                                if (Skill == null) {
                                    Skill = realm.createObject(skill.class, languageKnown);
                                    realm.copyToRealm(Skill);
                                }
                                employee.mSkills = new RealmList<>();
                                employee.mSkills.add(Skill);
                            }

                            realm.copyToRealm(employee);
                        }

                    } catch (RealmPrimaryKeyConstraintException e) {
                        Toast.makeText(getApplicationContext(), "Primary Key exists, Press Update instead", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } finally {
            if (realm != null) {
                realm.close();
            }
        }

    }


    private void readEmployeeRecords() {
        Realm realm = getRealm();
        setTextReadData(results);
    }

    private void changeData() {
      Realm realm =  getRealm();
       results = realm.where(Employee.class).findAll();

        results.addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<Employee>>() {
            @Override
            public void onChange(RealmResults<Employee> employees, OrderedCollectionChangeSet changeSet) {
                Toast.makeText(MainActivity.this, "data Change!!", Toast.LENGTH_SHORT).show();
               setTextReadData(results);
               setTextAge(results);

            }
        });

    }

    private Realm getRealm() {
        Realm realm = Realm.getDefaultInstance();
      return realm;
    }


    @Override
    protected void onStart() {
        super.onStart();
        changeData();
    }

    private void setTextReadData(RealmResults<Employee> results) {
        mTextView.setText("");
        for (Employee employee : results) {
            mTextView.append(employee.name + " ** age :  " + employee.age + " ** skill :  " + employee.mSkills.size() + "\n");
        }
    }


    private void updateEmployeeRecords() {
        Realm realm = getRealm();


        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {


                if (!inName.getText().toString().trim().isEmpty()) {


                    Employee employee = realm.where(Employee.class).equalTo(Employee.PROPERTY_NAME, inName.getText().toString()).findFirst();
                    if (employee == null) {
                        employee = realm.createObject(Employee.class, inName.getText().toString().trim());
                    }
                    if (!inAge.getText().toString().trim().isEmpty())
                        employee.age = Integer.parseInt(inAge.getText().toString().trim());

                    String languageKnown = inSkill.getText().toString().trim();
                    skill skill = realm.where(skill.class).equalTo(com.example.realmexample.skill.PROPERTY_SKILL, languageKnown).findFirst();

                    if (skill == null) {
                        skill = realm.createObject(skill.class, languageKnown);
                        realm.copyToRealm(skill);
                    }


                    if (!employee.mSkills.contains(skill))
                        employee.mSkills.add(skill);

                }
            }
        });


    }

    private void deleteEmployeeRecord() {
        Realm realm = getRealm();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Employee employee = realm.where(Employee.class).
                        equalTo(Employee.PROPERTY_NAME, inName.getText().toString()).findFirst();
                if (employee != null) {
                    employee.deleteFromRealm();
                }
            }
        });

    }


    private void filterByAge() {
        Realm realm = getRealm();


        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                RealmResults<Employee> results = realm.where(Employee.class).greaterThanOrEqualTo(Employee.PROPERTY_AGE, 25).findAllAsync();
                setTextAge(results);
            }
        });

    }

    private void setTextAge(RealmResults<Employee> results) {
        txtFilterByAge.setText("");
        for (Employee employee : results) {
            txtFilterByAge.append(employee.name + " age: " + employee.age + " skill: " + employee.mSkills.size() + "\n");
        }
    }


    private void checkStoragePermissions(Activity activity) {
        int permission = ActivityCompat.checkSelfPermission
                (activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE
            );
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mRealm != null) {
            mRealm.close();
        }
    }
}