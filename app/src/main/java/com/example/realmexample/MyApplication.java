package com.example.realmexample;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(getApplicationContext());

        RealmConfiguration configuration =
                new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                        .allowWritesOnUiThread(true)
                .build();

        Realm.setDefaultConfiguration(configuration);
    }
}
